﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Singleton to manage all the game
/// </summary>
public class GameManager : MonoBehaviour {
    #region Private Fields
    [SerializeField]
    private StateMachine stateMachineUI;

    [Header("Please insert the player prefab here")]
    [SerializeField]
    private GameObject playerPrefab;
    [SerializeField]
    private Transform playerStart;

    private GameObject playerInstance;

    #endregion

    #region Static Fields

    public static GameManager instance = null;

    #endregion

    void Awake()
    {
        //Singleton instantiation
        if (instance == null)
        {
            instance = this;
        }
        else
        { Destroy(this); } 
    }

	// Use this for initialization
	void Start () {
        //Register to menu state machine event
        stateMachineUI.OnChangedState.AddListener(UiStateMachineHandler);
    }
	
	// Update is called once per frame
	void Update () {

        
	}

    /// <summary>
    /// Manage states changes of Ui state machine
    /// </summary>
    /// <param name="_newState">State </param>
    private void UiStateMachineHandler(State _newState)
    {
        //Manage player  instanciation when game start
        if (_newState.GetType() == typeof(GameUIState))
        {
            //TODO: Create a class for the player start and find it in the scene to remove the reference (Allow that system to work between two scenes)
            if (playerStart == null)
                Debug.LogError("Please add a player start reference");

            playerInstance = Instantiate(playerPrefab, playerStart.position,playerStart.rotation);

            Debug.Log("game ui state ");
        }

        //TODO: Add a pause Ui menu between ui game menu and ui main menu.


        //TODO: Add a system to switch from one scene to another without detroying state machine.
        //Detroy player when return to main menu
        if (_newState.GetType() == typeof(MainMenuUIState))
        {
            if (playerInstance != null)
                DestroyImmediate(playerInstance);
        }
    }
}
