﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Player behaviour.
/// </summary>
[RequireComponent(typeof(Rigidbody))]
public class PlayerBehaviour : MonoBehaviour {

    [SerializeField]
    private PlayerInfo player;

    /// <summary>
    /// Move the player based on walk speed.
    /// </summary>
    /// <param name="_direction">Euler orientation of the action</param>
    public void Walk(Vector3 _direction)
    {
        move(_direction, player.walkSpeed);
    }

    /// <summary>
    /// Move the player based on run speed 
    /// </summary>
    /// <param name="_direction">Euler orientation of the action</param>
    public void Run(Vector3 _direction)
    {
        move(_direction, player.runSpeed);
    }

    /// <summary>
    /// Stop the player rigidbody
    /// </summary>
    public void Stop()
    {
        this.GetComponent<Rigidbody>().velocity = Vector3.zero;
        Debug.Log("stop");
    }

    /// <summary>
    /// Set palyer movement based on rigidbody.
    /// </summary>
    /// <param name="_direction"></param>
    /// <param name="speed"></param>
    private void move(Vector3 _direction, float speed)
    {
        this.GetComponent<Rigidbody>().velocity = _direction * speed;
    }

    
}
