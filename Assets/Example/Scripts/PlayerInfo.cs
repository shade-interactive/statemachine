﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInfo : MonoBehaviour {

    #region Public fields
    [SerializeField]
    public GameObject playerMesh;

    [SerializeField]
    public Camera playerCamera;

    [SerializeField]
    public float walkSpeed = 1;

    [SerializeField]
    public float runSpeed = 3;

    #endregion

}
