﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsUIState : UIState {

    #region Private fields
    [Header("States")]
    [SerializeField]
    private MainMenuUIState MainMenuUIState;

    [Header("UI")]
    [SerializeField]
    private Button BackButton;

    private bool switchToMainMenuUIState;
    #endregion

    #region State Behaviour
    public override void StartState()
    {
        switchToMainMenuUIState = false;
        BackButton.onClick.AddListener(() => { switchToMainMenuUIState = true; });

        base.StartState();
    }

    public override State UpdateSate()
    {
        if (switchToMainMenuUIState)
        {
            return MainMenuUIState;
        }

        return base.UpdateSate();
    }

    public override void EndState()
    {
        BackButton.onClick.RemoveAllListeners();
        base.EndState();
    }
    #endregion

}
