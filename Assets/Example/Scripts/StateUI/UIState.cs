﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIState : State {
    #region Private fields
    [Header("Top level UI Panel of that state")]
    [SerializeField]
    private RectTransform panelUiState;
    [SerializeField]
    private bool menuCameraRequested = false;

    private GameObject cam;
    #endregion

    #region State Behaviour
    public override void InitState()
    {
        panelUiState.gameObject.SetActive(false);

       
    }

    public override void StartState()
    {
        panelUiState.gameObject.SetActive(true);

        //TODO: Check if i loose one frame when I destroy then recreate camera. 
        //Manage new camera in the scene
        if (menuCameraRequested)
        {
            cam = new GameObject("MenuCamera");
            cam.AddComponent<Camera>();
        }
    }

    public override void EndState()
    {
        if (cam != null && menuCameraRequested)
        {
            DestroyImmediate(cam);
        }
       
        panelUiState.gameObject.SetActive(false);
    }

    #endregion
}
