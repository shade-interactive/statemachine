﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameUIState : UIState {
    #region Private fields
    [Header("States")]
    [SerializeField]
    private State MainMenuState;

    [Header("UI")]
    [SerializeField]
    private Button HomeButton;

    private bool SwitchToMainMenu;
    #endregion

    #region State Behaviour
    public override void StartState()
    {
        SwitchToMainMenu = false;
        HomeButton.onClick.AddListener(() => { SwitchToMainMenu = true; });
        base.StartState();
    }

    public override State UpdateSate()
    {
        if (SwitchToMainMenu)
        {
            return MainMenuState;
        }

        return base.UpdateSate();
    }

    public override void EndState()
    {
        HomeButton.onClick.RemoveAllListeners();
        base.EndState();
    }
    #endregion
}
