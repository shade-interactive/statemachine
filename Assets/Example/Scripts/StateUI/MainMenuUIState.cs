﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class MainMenuUIState : UIState {

    #region Private fields
    [Header("States")]
    [SerializeField]
    private GameUIState GameState;
    [SerializeField]
    private SettingsUIState SettingsState;
    [Header("UI")]
    [SerializeField]
    private Button StartButton;
    [SerializeField]
    private Button SettingsButton;
    
    private bool switchToGameState;
    private bool switchToSettingState;
    #endregion

    #region State Behaviour
    public override void StartState()
    {
        //PanelMainMenu.gameObject.SetActive(true);
        switchToGameState = false;
        switchToSettingState = false;

        StartButton.onClick.AddListener(()=> { switchToGameState = true; });
        SettingsButton.onClick.AddListener(()=> { switchToSettingState = true; });
        base.StartState();
    }

    public override State UpdateSate()
    {
        if (switchToGameState)
        {
            return GameState;
        }

        if (switchToSettingState)
        {
            return SettingsState;
        }

        return base.UpdateSate();
    }

    public override void EndState()
    {
        //PanelMainMenu.gameObject.SetActive(false);
        StartButton.onClick.RemoveAllListeners();

        base.EndState();
    }
    #endregion

}
