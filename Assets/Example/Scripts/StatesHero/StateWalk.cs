﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class StateWalk : StatePlayer{

    #region Private Fields
    [Header("States")]
    [SerializeField]
    private State stateIdle;
    [SerializeField]
    private State stateRun;

    #endregion

    #region State behaviour

    public override void StartState()
    {
       
        base.StartState();
    }

    public override State UpdateSate()
    {
        if (InputManager.instance.Horizontal == 0 && InputManager.instance.Vertical == 0)
        {
            return stateIdle;
        }

        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            return stateRun;
        }

        player.playerMesh.transform.eulerAngles = new Vector3(0, InputManager.instance.DirectionAngle, 0);
        player.GetComponent<PlayerBehaviour>().Walk(player.playerMesh.transform.forward);

        return base.UpdateSate();
    }

    public override void EndState()
    {
        base.EndState();
    }
    #endregion

}
