﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 
/// </summary>
public class StateRun : StatePlayer {

    #region private fields
    [Header("States")]
    [SerializeField]
    private State stateIdle;
    [SerializeField]
    private State stateWalk;
    

    #endregion

    #region State behaviour
    public override State UpdateSate()
    {
        if (InputManager.instance.Horizontal == 0 && InputManager.instance.Vertical == 0)
        {
            return stateIdle;
        }

        if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            return stateWalk;
        }

        player.playerMesh.transform.eulerAngles = new Vector3(0, InputManager.instance.DirectionAngle, 0);
        player.GetComponent<PlayerBehaviour>().Run(player.playerMesh.transform.forward);
        
        return base.UpdateSate();
    }

    #endregion

    
}
