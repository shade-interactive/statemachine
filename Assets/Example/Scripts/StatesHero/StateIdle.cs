﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Player is located on the ground and no input is used.
/// </summary>
public class StateIdle : StatePlayer {

    #region Private Fields
    [Header("Sates")]
    [SerializeField]
    private State stateWalk;
    [SerializeField]
    private State stateRun;
    
    /*
    [SerializeField]
    private float animationDuration;
    [SerializeField]
    private float maxHidleHeight;
    [SerializeField]
    private float minHidleHeight;
  
    [Header("Please keep curve time between 0 and 1")]
    [SerializeField]
    private AnimationCurve upCurve;
      */

    private bool IsAnimatingUp;
    private float StartTime;
    private float EndTime;
    private float normTime;
    private float normWithCurve;
    #endregion

    #region State Behaviour
    public override void StartState()
    {
        player.GetComponent<PlayerBehaviour>().Stop();

        base.StartState();      
    }

    public override State UpdateSate()
    {
        //TODO: Add that information in the input manager
        if(InputManager.instance.Horizontal != 0 || InputManager.instance.Vertical != 0)
        {
            if (Input.GetKey(KeyCode.LeftShift))
            {
                return stateRun;
            }

            return stateWalk;
        }


        /*
        if (Input.GetKeyDown(KeyCode.A) && !IsAnimatingUp)
        {
            IsAnimatingUp = true;
            StartTime = Time.time;
            EndTime = Time.deltaTime + AnimationDuration;
        }
        */

        //TODO: Add animation system of the player.
        /*
        if (IsAnimatingUp)
        {
            
            normTime = ((Time.time - StartTime) / AnimationDuration);
            normWithCurve = normTime;

            if (UpCurve != null)
            {
                //ToDo manage if the size of the curve change
                normWithCurve = UpCurve.Evaluate(normTime);
            }
 
            Hero.transform.position = new Vector3(Hero.transform.position.x, Mathf.Lerp(MinHidleHeight, MaxHidleHeight, normWithCurve), Hero.transform.position.z);
            if (normTime >= 1)
            {
                IsAnimatingUp = false;
            }

        }
        else
        {
            
        }
        */
        return base.UpdateSate();
    }

    public override void EndState()
    {
        IsAnimatingUp = false;

        base.EndState();
    }
    #endregion
}


