﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// All inputs will go through that class before being read by the player. (Singleton)
/// </summary>
public class InputManager : MonoBehaviour {

    #region public variables
    private float horizontal;

    public float Horizontal
    {
        get { return horizontal; }
        private set { horizontal = value; }
    }

    private float vertical;

    public float Vertical
    {
        get { return vertical; }
        private set { vertical = value; }
    }
    
    private float directionAngle;

    public float DirectionAngle
    {
        get { return directionAngle; }
        private set { directionAngle = value; }
    }
    #endregion

    #region public static variables 
    public static InputManager instance;
    #endregion

    #region Unity Behaviour
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            DestroyImmediate(this);
        }
    }

    // Use this for initialization
    void Start () {
        DirectionAngle = 0;
    }
	
	// Update is called once per frame
	void Update () {
      
        Horizontal = Input.GetAxisRaw("Horizontal");
        Vertical = Input.GetAxisRaw("Vertical");

        DirectionAngle = Mathf.Atan2(horizontal, vertical) * Mathf.Rad2Deg;
    }
    #endregion
}
