﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class StateChangedEvent : UnityEvent<State>
{ }

/// <summary>
/// State machine manage several states and allows to switch from one to another.
/// </summary>
public class StateMachine : MonoBehaviour {

    #region Private fields

    [SerializeField]
    private List<State> states;

    #endregion

    #region Properties

    private State currentState;

    public State CurrentState
    {
        get { return currentState; }
        private set { currentState = value; }
    }

    private State previousState;

    public State PreviousState
    {
        get { return previousState; }
        private set { previousState = value; }
    }
    
    #endregion

    #region Events

    public StateChangedEvent OnChangedState;

    #endregion


    //TODO: Add a play - stop - pause system to manage state machine from another state machine if needed.

    #region Unity execution

    void Awake()
    {
        //All states init
        foreach (State state in states)
        {
            state.InitState();
        }
    }

	void Start () {

        if (OnChangedState == null)
        {
            OnChangedState = new StateChangedEvent();
        }
        //First state Start
        CurrentState = states[0];
        CurrentState.StartState();
	}

	void Update () {
        //Current state update
        State nextState = currentState.UpdateSate();

        if (nextState != null)
        {
            ChangeState(nextState);
        }
	}

    #endregion

    #region Private methods

    private void ChangeState(State _newState)
    {
        CurrentState.EndState();
        PreviousState = CurrentState;
        CurrentState = _newState;

        OnChangedState.Invoke(_newState);

        CurrentState.StartState();
    }

    #endregion
}
