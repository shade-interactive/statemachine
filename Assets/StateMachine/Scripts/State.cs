﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Use state as a parent to create your own state managed by state machine.
/// </summary>
public abstract class State : MonoBehaviour {

    #region Private field

    protected State baseNextState = null;

    #endregion


    #region Virtual methods
    /// <summary>
    /// Init state on awake of the state machine.
    /// </summary>
    public virtual void InitState()
    {


    }

    /// <summary>
    /// Start state is called by the state machine when new previous state has ended.
    /// </summary>
    public virtual void StartState()
    {
        baseNextState = null;
        Debug.Log("Start New State : " + this.GetType().Name);
    }


    /// <summary>
    /// Call on every frame by the state machine.
    /// </summary>
    /// <returns>Next state (Null for current state)</returns>
    public virtual State UpdateSate()
    {
        return baseNextState;
    }

    /// <summary>
    /// Called by state machine when this the state has finished and new state has been called. 
    /// </summary>
    public virtual void EndState()
    {

    }

    #endregion
}
