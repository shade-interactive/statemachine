# README #

### What is this repository for? ###

* Quick summary: Basic State machine implementation in unity 3D that allows to switch from different states easily.
* Version : 0.01

### How do I get set up? ###

* Summary of set up: State machine can be setup on any object. Overide virtual class state to create different states.
* How to run tests: You can see example of a state machine in "../Assets/Example/ExampleStateMachine"

### Next steps ###

* Player: Implement movement behavirou of the player.
* Player: Add animation on the player.
* State machine: Add transition to state machine.
* State machine: Add play / Pause / Stop to state machine.

### Contribution guidelines ###

### Who do I talk to? ###